## SEGUNDO COMPILADOR

João Mauad Seckler
no USP 4603521


# Como executar

`$ make && ./mcalc | ./direto`


# Como testar

`make && ./teste`


# A sintaxe

Adotamos uma notação infixa simples. Como de costume, os parênteses
permitem explicitar a precedência das operações, que, doutro modo,
funcionam como é usual.

A condicional se nota pelo chamado "operador ternário" [ternary
operator], e é usada da seguinte maneira:

`<condição> ? <expressão 1> : <expressão 2>`

Que resulta na avaliação da expressão 1, caso a avaliação da condição
devolva zero, e da expressão 2, caso contrário. A condição, por sua vez,
é só mais outra expressão.

Depois, definimos 4 operações:

- `def(nome, arg, exp)` define uma função de nome `nome`, definida no
  argumento `arg`, avaliada de acordo com a expressão `exp`. Só são
  permitidas funções de um argumento. Funções assim definidas podem ser
  chamadas recursivamente e guardam consigo o nome de espaço do momento
  em que foram definidas ("closure").

- `call(nome, argumento)` chama uma função de nome `nome` avaliada no
  argumento `argumento`.

- `let(var, val, expr)` declara uma variável var, incializada com o
  valor resultante da avaliação da expressão `val`, e, em seguida,
  executa a expressão `expr` com o ambiente incluindo essa variável.

- `set(var, val)` atualiza o valor de uma variável `var` com o resultado
  da avaliação da expressão val.


Além disso, um ponto e vírgula (";") separa n expressões a serem
avaliadas uma em seguida da outra. Essas expressões não compartilham o
espaço de nomes.


# Dependências:

 - gcc
 - raco
 - bash
 - bison/yacc
 - flex/lex
 - make
