CC = gcc -Wall -pedantic -O2

tudo: mcalc direto

mcalc: mcalc.tab.o lex.yy.o main.o
	${CC} -o $@ $^  -lfl

mcalc.tab.o: mcalc.y
	bison -d mcalc.y
	${CC} -c mcalc.tab.c

lex.yy.o: mcalc.l
	flex mcalc.l
	${CC} -c lex.yy.c

direto: direto.rkt
	raco exe $<

clean:
	rm -f *.o lex.yy.c mcalc.tab.c mcalc.tab.h direto mcalc
