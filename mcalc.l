D [0-9]
E [Ee][+-]?{D}+
W [a-zA-Z]
WD [a-zA-Z0-9]

%{
#include <math.h>
#include <string.h>
#include "mcalc.tab.h"

%}


%%
{D}*[.]?{D}+{E}?    { yylval.val = yytext; return NUM; }

[ \t\n]+    /* pula espacos em branco */

#.*        /* comentarios simples */

[(]           { return OPEN; }
[)]           { return CLOSE; }
[+]           { return ADD; }
[-]           { return SUB; }
[*]           { return MUL; }
[/]           { return DIV; }
[?]           { return COND; }
[:]           { return COLON; }
[,]           { return COMMA; }
def           { return DEF; }
call          { return CALL; }
let           { return LET; }
seq           { return SEQ; }
set           { return SET; }
[;]           { return SEMICOLON; }

{W}+{WD}* { yylval.val = malloc(strlen(yytext) + 1);
            strcpy(yylval.val, yytext); return NAME; }

.    {  fprintf(stderr, "Entrada ignorada\n--> %s\n", yytext); }

%%

