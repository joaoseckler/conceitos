#lang plai-typed


; Recalling the grand scheme of things:
; s-exp -------> arithS -------> ExprC ------> result
;       parser          desugar         interp

; Core type
(define-type ExprC
  [numC (n : number)]
  [plusC (l : ExprC) (r : ExprC)]
  [multC (l : ExprC) (r : ExprC)]
  [divC (l : ExprC) (r : ExprC)]
  [ifC (c : ExprC) (y : ExprC) (n : ExprC)]
  [idC (s : symbol)]
  [lamC (arg : symbol) (body : ExprC)]
  [appC (fun : ExprC) (a : ExprC)]
  [seqC (b1 : ExprC) (b2 : ExprC)]

  ; All the boxing becomes one thing
  [setC (var : symbol) (arg : ExprC)]
  )

; Sugared type
(define-type ExprS
  [numS (n : number)]
  [plusS (l : ExprS) (r : ExprS)]
  [bminusS (l : ExprS) (r : ExprS)]
  [uminusS (e : ExprS)]
  [multS (l : ExprS) (r : ExprS)]
  [divS (l : ExprS) (r : ExprS)]
  [ifS (c : ExprS) (y : ExprS) (n : ExprS)]
  [idS (s : symbol)]
  [lamS (arg : symbol) (body : ExprS)]
  [appS (s : ExprS) (a : ExprS)]
  [seqS (b1 : ExprS) (b2 : ExprS)]
  [setS (var : symbol) (arg : ExprS)]

  ; This will allow for recursion
  [letS (id : symbol) (val : ExprS) (body : ExprS)]
  )


; Turn sugar into core
(define (desugar [as : ExprS]) : ExprC
  (type-case ExprS as
    [numS (n) (numC n)]
    [plusS (l r) (plusC (desugar l) (desugar r))]
    [multS (l r) (multC (desugar l) (desugar r))]
    [divS (l r) (divC (desugar l) (desugar r))]
    [bminusS (l r) (plusC (desugar l) (multC (numC -1) (desugar r)))]
    [uminusS (e) (multC (numC -1) (desugar e))]
    [ifS (c y n) (ifC (desugar c) (desugar y) (desugar n))]
    [idS (s) (idC s)]
    [appS (s a) (appC (desugar s) (desugar a))]
    [lamS (a b) (lamC a (desugar b))]
    [seqS (b1 b2) (seqC (desugar b1) (desugar b2))]
    [setS (v a) (setC v (desugar a))]
    [letS (id val expr) (appC (lamC id (desugar expr)) (desugar val))]
    ))


; -------- RETURN VALUES ---------------------------------------------------

; Values of our language and associated functions

(define-type Value
  [numV (n : number)]
  [closV (arg : symbol) (body : ExprC) (env : Env)]
  ; The role of the box is encapsulated under variable, thus, we don't
  ; need this value anymore
  ; [boxV (l : Location)]
  )

; Now we want to return a value and a store, but a function only returns
; one value, thus this data structure
(define-type Result
             [v*s (v : Value) (s : Store)])


; This is an example of "safe run-type system"
(define (num+ [l : Value] [r : Value]) : Value
  (cond
    [(and (numV? l) (numV? r))
     (numV (+ (numV-n l) (numV-n r)))]
    [else (error 'num* "argument was not a number")]
    )
  )

(define (num* [l : Value] [r : Value]) : Value
  (cond
    [(and (numV? l) (numV? r))
     (numV (* (numV-n l) (numV-n r)))]
    [else (error 'num* "argument was not a number")]
    )
  )

(define (num/ [l : Value] [r : Value]) : Value
  (cond
    [(and (numV? l) (numV? r))
     (numV (/ (numV-n l) (numV-n r)))]
    [else (error 'num* "argument was not a number")]
    )
  )
; ---------------------------------------------------------------------------


; Main interpreter.
; New function definition! we take a store and return a Result now
; Changing the return type changes everything!
(define (interp [a : ExprC] [env : Env] [sto : Store]) : Result
  (type-case ExprC a
    [numC (n) (v*s (numV n) sto)]

    ; right operands need to receive the left operand's store
    [plusC (l r) (type-case Result (interp l env sto)
                   [v*s (v-l s-l)
                     (type-case Result (interp r env s-l)
                       [v*s (v-r s-r)
                         (v*s (num+ v-l v-r) s-r)])])]

    [multC (l r) (type-case Result (interp l env sto)
                   [v*s (v-l s-l)
                     (type-case Result (interp r env s-l)
                       [v*s (v-r s-r)
                         (v*s (num* v-l v-r) s-r)])])]

    [divC (l r) (type-case Result (interp l env sto)
                   [v*s (v-l s-l)
                     (type-case Result (interp r env s-l)
                       [v*s (v-r s-r)
                         (v*s (num/ v-l v-r) s-r)])])]

    [ifC (c y n) (type-case Result (interp c env sto)
                    [v*s (v-c s-c)
                         (if (zero? (numV-n v-c))
                           (interp n env s-c)
                           (interp y env s-c))])]

    ; the compisition of fetch and lookup equals the old lookup
    [idC (s) (v*s (fetch (lookup s env) sto) sto)]
    [lamC (a b) (v*s (closV a b env) sto)]
    [appC (f a)
          (type-case Result (interp f env sto)
             [v*s (v-f s-f)
                  (type-case Result (interp a env s-f)
                     [v*s (v-a s-a)
                          (let ([where (new-loc)])
                            (interp (closV-body v-f)
                                    (extend-env (bind (closV-arg v-f)
                                                      where)
                                                (closV-env v-f))
                                    (override-store (cell where v-a)
                                                    s-a)))])])]

    ; Use store from the evaluation of b1 to evaluate b2!
    [seqC (b1 b2) (type-case Result (interp b1 env sto)
                    [v*s (v-b1 s-b1) (interp b2 env s-b1)])]

    [setC (var val) (type-case Result (interp val env sto)
                      [v*s (v-val s-val)
                           (let ([where (lookup var env)])
                             (v*s v-val
                                  (override-store (cell where v-val)
                                                  s-val)))])]

    ))


; -------- ENVIRONMENT/STORE ----------------------------------------------

; Now we have the environment and store working together. The
; environment links a name to a location. The store links the location
; to the value. This way, we can implement mutation. It works as if
; locations were addresses and we could only change variables through
; pointers.


(define-type-alias Location number)

; Env
(define-type Binding
             [bind (name : symbol) (val : Location)])
(define-type-alias Env (listof Binding))
(define mt-env empty)
(define extend-env cons)

(define (lookup [for : symbol] [env : Env]) : Location
  (cond
    [(empty? env) (error 'lookup "name not found")]
    [(symbol=? for (bind-name (first env))) (bind-val (first env))]
    [else (lookup for (rest env))]))


; Store
(define-type Storage
             [cell (location : Location) (val : Value)])
(define-type-alias Store (listof Storage))
(define mt-store empty)
; In fact, cons properly prepends another bind to the Store. We are
; calling this overriding, because we will never look to the rest of the
; list. It could work like a trace; it keeps record of what has been
; kept in that location
(define override-store cons)

; equivalent to lookup
(define (fetch [loc : Location] [sto : Store]) : Value
  (cond
    [(empty? sto) (error 'fetch "location not found")]
    [(= loc (cell-location (first sto))) (cell-val (first sto))]
    [else (fetch loc (rest sto))]))


; ----------------------------------------------------------------------




; Parse more or less regular lisp expressions to our language,
; just so we don't have to tipe everything with ArtihC or ExprS types.
(define (parse [s : s-expression]) : ExprS
  (cond
    [(s-exp-number? s) (numS (s-exp->number s))]
    [(s-exp-symbol? s) (idS (s-exp->symbol s))]
    [(s-exp-list? s)
      (let ([sl (s-exp->list s)])
        (case (s-exp->symbol (first sl))
          [(+) (plusS (parse (second sl)) (parse (third sl)))]
          [(*) (multS (parse (second sl)) (parse (third sl)))]
          [(-) (bminusS (parse (second sl)) (parse (third sl)))]
          [(/) (divS (parse (second sl)) (parse (third sl)))]
          [(~) (uminusS (parse (second sl)))]
          [(if) (ifS (parse (second sl)) (parse (third sl))
                     (parse (fourth sl)))]
          [(call) (appS (parse (second sl)) (parse (third sl)))]

          ; usage: func name argument body
          [(func) (letS (s-exp->symbol (second sl))
                        (numS 0) ; dummy
                        (setS (s-exp->symbol (second sl))
                                (lamS (s-exp->symbol (third sl))
                                      (parse (fourth sl))))
                          )]


          [(set) (setS (s-exp->symbol (second sl)) (parse (third sl)))]
          [(seq) (seqS (parse (second sl)) (parse (third sl)))]
          [(def) (letS (s-exp->symbol (second sl))
                       (parse (third sl)) (parse (fourth sl)))]

          [else (error 'parse "invalid list input")]))]
    [else (error 'parse "invalid input")]))

; ----------------------------------------------------------------------

; Returns next available location
(define new-loc
   (let ([n (box 0)])
        (lambda ()
           (begin
              (set-box! n (+ 1 (unbox n)))
              (unbox n)))))



; (define (run x) (interp (desugar (parse x)) mt-env mt-store))
; Read from standard input!
(interp (desugar (parse (read))) mt-env mt-store)
