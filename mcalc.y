%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *oper(char op, char *l, char *r) {
    char *res = malloc(strlen(l)+strlen(r)+6);
    sprintf(res, "(%c %s %s)", op, l, r);
    return res;
}

char *catenate3(char *st, char *nd, char *rd) {
    char *res = malloc(strlen(st) + strlen(nd) + strlen(rd) + 6);
    sprintf(res, "(%s %s %s)", st, nd, rd);
    return res;
}

char *catenate4(char *st, char *nd, char *rd, char *th) {
    char *res = malloc(strlen(st) + strlen(nd) + strlen(rd) + strlen(th) + 7);
    sprintf(res, "(%s %s %s %s)", st, nd, rd, th);
    return res;
}

char *dup(char *orig) {
    char *res = malloc(strlen(orig)+1);
    strcpy(res,orig);
    return res;
}

int yylex();
void yyerror(char *);
%}

%union {
    char *val;
}

%token    <val> NUM NAME
%token  ADD SUB MUL DIV OPEN CLOSE COND COLON DEF SEMICOLON CALL LET COMMA SEQ SET
%type    <val> exp

%left SEMICOLON
%left NAME
%left DEF
%left ADD SUB
%left MUL DIV
%left NEG
%left COND

/* Grammar */
%%

input:
    |  exp                    { puts($1); }
    |  error                  { fprintf(stderr, "Invalid input\n"); }
;

exp:   NUM                    { $$ = dup($1); }
    |  exp ADD exp            { $$ = oper('+', $1, $3); }
    |  exp SUB exp            { $$ = oper('-', $1, $3); }
    |  exp MUL exp            { $$ = oper('*', $1, $3); }
    |  exp DIV exp            { $$ = oper('/', $1, $3); }
    |  SUB exp %prec NEG      { $$ = oper('~', $2, ""); }
    |  OPEN exp CLOSE         { $$ = dup($2); }
    |  exp COND exp COLON exp { $$ = catenate4("if", $1, $3, $5); }
    |  DEF OPEN NAME COMMA NAME COMMA exp CLOSE
                              { $$ = catenate4("func", $3, $5, $7); }
    |  CALL OPEN exp COMMA exp CLOSE
                              { $$ = catenate3("call", $3, $5); }
    |  LET OPEN NAME COMMA exp COMMA exp CLOSE
                              { $$ = catenate4("def", $3, $5, $7); }
    |  SET OPEN NAME COMMA exp CLOSE
                              { $$ = catenate3("set", $3, $5); }
    |  exp SEMICOLON exp      { $$ = catenate3("seq", $1, $3); }
    |  NAME                   { $$ = $1; }
;

%%

void yyerror(char *s) {
  fprintf(stderr,"%s\n",s);
}
